package ictu.edu.vn.android.viewpage;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DealMonthFragment extends Fragment {
    public static final String KEY_MONTH = "month";

    public static DealMonthFragment newInstance(int month) {
        DealMonthFragment fragment = new DealMonthFragment();
        Bundle args = new Bundle();
        args.putInt(KEY_MONTH, month);
        fragment.setArguments(args);
        return fragment;
    }

    private int month = 1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        month = getArguments().getInt(KEY_MONTH);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup vg, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.deal_month, vg, false);

        TextView tvMonth = view.findViewById(R.id.tvMonth);
        tvMonth.setText("Tháng " + month);

        return view;
    }
}

