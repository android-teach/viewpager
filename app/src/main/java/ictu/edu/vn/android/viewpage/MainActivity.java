package ictu.edu.vn.android.viewpage;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        PageAdapter adapter = new PageAdapter(getSupportFragmentManager());

        for (int i = 1; i <= 12; i++) {
            adapter.addFragment(DealMonthFragment.newInstance(i));
        }

        ViewPager viewPager = findViewById(R.id.viewPager);
        viewPager.setAdapter(adapter);
    }
}

